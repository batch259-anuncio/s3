<?php
class Building{

    public string $name;
    public int $floors;
    public string $address;

    public function setName($name): void
    {
         $this->name = $name;
    }
    
    public function getName(): string
    {
        return $this->name;
    }

    public function setFloors($floors): void
    {
         $this->floors = $floors;
    }
    
    public function getFloors(): int
    {
        return $this->floors;
    }
    public function setAddress($address): void
    {
         $this->address = $address;
    }

    public function getAddress(): string
    {
        return $this->address;
    }
  

}


class Condominium extends Building{
       
 
}

$building = new Building();
$condomonium = new Condominium();