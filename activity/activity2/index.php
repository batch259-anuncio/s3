<?php require_once './code.php' ?>

<?php 
    $building->setName('Caswynn Building');
    $building->setFloors(8);
    $building->setAddress('Timog Avenue, Quezon City, Philippines');

    $condomonium->setName('Enzo Condo');
    $condomonium->setFloors(5);
    $condomonium->setAddress('Buendia Avenue, Makati City, Philipphines')
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>activity2 </title>
</head>
<body>

<h1>Building</h1>

<p><?= "The name of the building is " . $building->getName() ?></p>
<p><?= "The " . $building->getName() . " has " .  $building->getFloors(). " floors"?></p>
<p><?= "The " . $building->getName() . " is located at " . $building->getAddress() ?></p>

<?php $building->setName('Caswynn Complex') ?>
<p><?= "The name of the building has changed to " . $building->getName() ?></p>

<h1>Condominium</h1>
<p><?= "The name of the building is " . $condomonium->getName() ?></p>
<p><?= "The " . $condomonium->getName() . " has " .  $condomonium->getFloors(). " floors"?></p>
<p><?= "The " . $condomonium->getName() . " is located at " . $condomonium->getAddress() ?></p>

<?php $condomonium->setName('Enzo Tower') ?>
<p><?= "The name of the condomonium has changed to " . $condomonium->getName() ?></p>
    
</body>
</html>