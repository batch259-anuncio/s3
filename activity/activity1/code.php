<?php

class Person{
    public  $firstName;
    public  $middleName;
    public  $lastName;

    public function __construct($firstName, $middleName, $lastName){
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    public function printName(){
        return "Your full name is  $this->firstName $this->lastName";
    }
    
}

class Developer extends Person{
    public function printName(){
        return "Your full name is  $this->firstName $this->lastName and you are a developer";
    }
}

class Engineer extends Person{
    public function printName(){
        return "Your are an engineer  $this->firstName $this->lastName";
    }
}

$person = new Person('Senku', ' ', 'Ishigami');
$engineer = new Engineer('Harold','Myers', 'Reese' );
$developer = new Developer('John', 'Finch', 'Smith');