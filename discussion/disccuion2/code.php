<?

class Building {

    // Access Modifiers
    // 1. public: fully open, property and methods can be accessed from anywhere
    // 2. private: 
        // disables direct access to an object's property or methods
        // private modifier also disable inheritance of its properties and methods to child class
        // method and property can only be accessed within the class


    public $name;
    public $floor;
    public $address;

    public function __construct($name, $floor, $address ) {

        $this->name = $name;
        $this->floor = $floor;
        $this->address = $address;
    }

}

class Condiminium extends Building{

}

$building = new Building('Caswynn Building', 8, 'Timog Ave., Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');