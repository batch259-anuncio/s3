<?php

// Objects as Variables

$buildingObj = (object) [
    'name' => 'Caswynn Building',
    'floor' => '8',
    'address' => 'Brgy. Sacred Heart, Quezon City, Philippines'
];

$buildingObj2 = (object) [
    'name' => 'GMA Network',
    'floor' => '20',
    'address' => 'Brgy. Sacred Heart, Quezon City, Philippines'
];

// Objects from Classes
class Building
{
    // Properties (instead of variables)
    // Characteristics of an object
    public $name;
    public $floor;
    public $address;

    // Constructor Function
    // A constructor allows us to initialize an object's properties upon creation of the object.
    public function __construct($name, $floor, $address)
    {
        $this->name = $name;
        $this->floor = $floor;
        $this->address = $address;
    }

    // Methods
    // An action that object can perform 
    public function printName()
    {
        return "The name of the building is $this->name.";
    }
}

$building = new Building('Caswynn Building', '8', 'Timog Ave., Quezon City, Philippines');


// Inheritance and Polymorphism

// Polymorphism - Methods inherited by a child/derived class can be overriden to have a behavior different from the method of the base/parent class.

class Condominium extends Building{
    public function __construct($name)
    {
        $this->name=$name;
    }
}