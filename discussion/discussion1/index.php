<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03A: Classes and Objects</title>
</head>

<body>
    <!-- objects created from variables -->
    <h1>Objects and Classes</h1>

    <h3>Object as Variables</h3>
    <p> <?php var_dump($buildingObj); ?></p>
    <p> <?php var_dump($buildingObj2); ?></p>
    <p> <?php echo $buildingObj->name; ?></p>

    <h3>Object created using Building Class</h3>
    <p> <?php var_dump($building); ?></p>
    <p> <?php echo $building->name; ?></p>
    <p> <?php echo $building->printName(); ?> </p>

    <h3>Modifying the instatiated Object</h3>
    <?php $building->name = 'GMA Network' ?>
    <p> <?php var_dump($building); ?></p>
    <p> <?php echo $building->printName(); ?> </p>
</body>

</html>